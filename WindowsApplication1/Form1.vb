﻿Imports RestSharp

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim client = New RestClient("http://localhost:53622")

        Dim request = New RestRequest("api/My", Method.GET)
        TextBox1.Text = client.Execute(request).Content
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim client = New RestClient("http://localhost:53622")

        Dim request = New RestRequest("api/My", Method.POST)
        request.RequestFormat = DataFormat.Json
        request.AddBody(TextBox2.Text)
        client.Execute(request)
    End Sub

End Class
